# diagnalScrapMaster

To install all dependencies
---------------------------

$ npm install

API to scrape an input URL and parse its metadata
-------------------------------------------------

URL : http://localhost:8080/scraper/getDetails

Method : POST

Request Body : {"url": input URL}
eg:
{
"url": "https://www.flipkart.com/sennheiser-cx213-wired-headphone/p/itmf3vhfhyuq4zdj?pid=ACCE8JXRCS8QVWGV&lid=LSTACCE8JXRCS8QVWGVTPINJ9&marketplace=FLIPKART&srno=b_1_2&otracker=hp_omu_Deals%2Bof%2Bthe%2BDay_3_AYLH1PGBZXV9_0&otracker1=hp_omu_PINNED_neo%2Fmerchandising_Deals%2Bof%2Bthe%2BDay_NA_dealCard_cc_3_NA_0&fm=neo%2Fmerchandising&iid=0b994fa4-66bb-4296-aeb9-11ab229ad0de.ACCE8JXRCS8QVWGV.SEARCH&ppt=StoreBrowse&ppn=Store&ssid=aupnj1251vzyuww01555867301325"
}

