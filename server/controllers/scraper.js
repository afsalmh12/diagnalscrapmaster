const express = require("express");
const router = express.Router();
const cheerio = require('cheerio');
const request = require('request');
const siteInfo = require('../models/siteInfo');
const promise = require('promise');
const ogs = require('open-graph-scraper');

function scrapUrl(url) {
    let info = siteInfo.info;
    return new promise(function (resolve, reject) {
        request(url, function (error, response, html) {
            if (!error && response && response.statusCode == 200) {
                var $ = cheerio.load(html);
                info.title = $("title").text();
                $('img').each(function (i, e) {
                    info.images[i] = $(this).attr("src")
                });
                $('p,h1,h2,h3,h4,h5,h6').each(function (i, e) {
                    let description = $(this).text();
                    info.descriptions.push(description)
                });
                resolve(info);
            } else {
                reject(error);
            }
        })

    })
}

function scrapOGP(url) {
    let info = siteInfo.info;
    return new promise(function (resolve, reject) {
        var options = { 'url': url, 'encoding': 'utf8' };
        ogs(options, function (error, results) {
            if (error)
                reject(error);
            else
                resolve(results);
        });
    })
}

router.post('/getDetails', function (req, res) {
    async function getDetails() {
        let url = req.body.url;
        try {
            let results = await scrapOGP(url);
            if (results.data == undefined || Object.keys(results.data).length == 0) {
                results = await scrapUrl(url)
            }
            res.json(results);
        }
        catch (e) {
            res.send("Failed to scrap the webpage")
        }

    }
    getDetails();
});
module.exports = router;