const express = require('express');
const bodyParser = require('body-parser');
const scraper = require('./server/controllers/scraper');
const port=8080;

//creating express app
const app = express();
//body-parser as middleware
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use('/scraper',scraper);
app.get('*',function(req,res){
res.send("Server started");
});
app.listen(port,function(){
    console.log("Server connected : "+port);
});